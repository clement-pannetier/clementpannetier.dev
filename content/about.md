+++
title = "A propos de moi"
slug = "a-propos"
+++

{{< figure src="/images/avatar-transparent.png" alt="Avatar Clément PANNETIER" >}}

Je m'appelle Clément PANNETIER et j’ai 25 ans.

Titulaire d’une Licence Professionnelle Développement Web et Mobile réalisée à l’[IUT d’Orléans](https://www.univ-orleans.fr/fr/) en 2018 / 2019, je l’ai effectuée en alternance au sein d’un organisme de formation nommé [Loging Formation](https://loging-formation.com/) – [adhara France](https://adhara.fr/).

Ma mission principale fût de mettre à jour leurs sites web développés sur [Joomla!](https://www.joomla.org/) ainsi que de développer de nouveaux outils permettant la gestion de leurs formations.

Cette expérience m’a enrichie humainement d’un point de vue relationnel de par ma position au sein d’une équipe de 9 collaborateurs. Elle m’a également permis de développer mon autonomie, ma réactivité dans la gestion des aléas, ma confiance en moi et ma capacité à prendre des initiatives à travers les tâches qui m’ont été confiées.

De plus, tout au long de ma formation, de nombreux projets notés ont étés réalisés comme par exemple un chatbot sur les cryptomonnaies, un service d'API de musiques, une application de gestion d'annuaire pour l'association ["La Fabrique Opéra Val de Loire"](http://www.lafabriqueopera-valdeloire.com/) ou encore une application mobile Android.

Voici un [lien](https://drive.google.com/open?id=1whd3h4IbvYYzEfO1el44-M6fdFLdwbJ7) permettant d'accéder au rapport de cette période d'apprentissage.

Depuis juin 2020, je fais désormais partie de l'aventure [AC3](https://www.ac3-groupe.com) en tant que développeur PHP.

Au sein du pôle Maintenance et parmi une équipe d'une dizaine de personnes, mes missions consistent principalement en la résolution de tickets, c'est à dire la compréhension et la résolution notamment d'erreurs sur le logiciel, mais aussi le développement de nouvelles fonctionnalités sur celui-ci.