+++
title = "Réalisations"
slug = "realisations"
+++

## Loging Formation - adhara France

### Missions

* Mise à jour des sites web (version de Joomla!, PHP, template, HTTPS)
* Développement de nouveaux outils pour la gestion de leurs formations
* Amélioration du référencement des deux sites (Google Analytics, Search Console...)
* Utilisation de la version 2 de l'API du CRM Zoho

### Langages / Librairies utilisés

* PHP 7
* HTML5 / CSS3
* JavaScript / JQuery / JQueryUI
* UIkit, Bootstrap, FontAwesome
